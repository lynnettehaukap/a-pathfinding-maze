//A* pathfinding maze that is hardcoded to work with face.bmp
//to use other bmp mazes initGraph() needs to be modified (data is not read from stdin)
//current implementation does not use nodes or matrix
//Note current a* algorithm only uses horizontal and vertical moves (diagonal are not legit moves)
//TODO write get parent using camefrom, change previous pathe to came from
  //      only use lowest fscore in open that is a valid neighbor of camefrom
    //    final path should be different than camefrom , create this in recreate path

#include <iostream>
#include "Graph.h"
#include <stdlib.h>
#include <SDL/SDL.h>
#include <map>
#include <vector>
#include <climits>
#include <cmath>
#include<queue>

using namespace std;

Graph::Graph()
{
    initGraph();
    initGameState();
}


void Graph::initGameState()
{
    initGraph();
    initGScore();
    initFScore();
}

//obtains a* path 
vector<int> Graph::gameEngine()
{
    aStar();
    vector<int> vec;
cout << "reconstruct" << endl;
    reconstructPath(vec);
//printPath();
    return vec;
}

//init gscore with max int
void Graph::initGScore()
{
    //init start gscore to 0
    gScore.insert(std::pair<int,int>(start, 0) );
    for(int i = 1; i < graphSize; ++i)
    {
        gScore.insert(std::pair<int, int>(i, INT_MAX) );
    }
}

void Graph::initFScore()
{
    //init start fscore: heuristic only
    fScore.insert(std::pair<int,int>(start, getHeuristic(start) ) );
    //init fscore to max int
    for(int i = 1; i < graphSize; ++i)
    {
        fScore.insert(std::pair<int, int>(i, INT_MAX) );    
    }
}

//estimate heuristic for a node
int Graph::getHeuristic(int neighbor)
{
    //estimate vertical, horizontal path to goal square    
    std::map<int, vector<int> >::iterator it = matrix.find(neighbor);
    std::map<int, vector<int> >::iterator it2 = matrix.find(goal);
    std::vector<int> vec = it->second;
    std::vector<int> vec2 = it2->second;
    int colMove = abs(vec[1] - vec2[1]);
    int rowMove = abs(vec[0] - vec2[0]);
    return colMove + rowMove;
}

//returns index to neighbor or -1 if not found
bool Graph::isOpen(int neighbor)
{
    for(int i = 0; i < open.size(); ++i)
    {
        if(open[i] == neighbor)
            return true;
    }
    return false;
}

void Graph::removeOpenNode(int neighbor)
{
    for(int i = 0; i< open.size(); ++i)
    {
        if(open[i] == neighbor)
            open.erase(open.begin() + i);
    }   
}

//is neighbor in closed list, true if found
bool Graph::isClosed(int neighbor)
{
    for(int i = 0; i < closed.size(); ++i)
    {
        if(closed[i] == neighbor)
        {
            return true;
        }
    }
    return false;
}
int Graph::getParent(int node)
{
    std::map<int, int>::iterator it = cameFrom.find(node);
    return it->second;
}

bool Graph::isNeighbor(int node)
{
    //if node is a neighbor of current then return true
    std::map<int, vector<int> >::iterator it2 = myGraph.find(current);
    std::vector<int> vec = it2->second;
    for(int i = 0; i < vec.size(); i++)
    {
        if(node == vec[i])
            return true;
    }
    return false;
}

//a* algorithm
bool  Graph::aStar()
{
    int preGscore = 0;
    //add start to open list
    open.push_back(0);
    
    while(open.size() != 0)
    {
        //if node is not start 
        current = getLowestFScoreNode();    
cout << "current " << current << endl;
        //goal reached
        if(current == goal)
        {
            return true;
        }
        
        //remove current from open list
        removeOpenNode(current);
        
        //add current to closed
        closed.push_back(current);

        std::map<int, vector<int> >::iterator iter = myGraph.find(current);
        std::vector<int> vec = iter->second;//neighbors list

        //for each open neighbor,calc pre-gscore 
        for(int i = 0; i < vec.size(); ++i)
        {
            
            //if neighbor closed, continue
            if(isClosed(vec[i]) || !isNeighbor(vec[i]) )
                continue;
            std::map<int, int>::iterator it = gScore.find(current);
            std::map<int, int>::iterator iter = gScore.find(vec[i]);
            //calculate pre-gscore: current's gscore++
            preGscore = it->second + 10;
        
            //if neighbor is not in open add to open
            if( !isOpen(vec[i]) )
            {
                open.push_back(vec[i]);
            }
            //if pre- gscore >= actual gscore, continue
            else if(preGscore >= iter->second)
                continue;
        
            std::map<int, int >::iterator it1 = gScore.find(vec[i]);
            std::map<int, int >::iterator it2 = fScore.find(vec[i]);
            //record the path
            setPreviousPath(vec[i]);
            //set gScore
            setGScore(it1->first, preGscore);
            //set fScore: gScore + heuristic
            setFScore(it2->first ,preGscore);
        }        
    }
return false;
}

void Graph::reconstructPathRecursive(vector<int>& vec, int node)
{
    if(node == start)
    {
        vec.push_back(node);
        return;
    }
    std::map<int, int>::iterator it = cameFrom.find(node);
    vec.push_back(node);
    reconstructPathRecursive(vec, it->second);
}

void Graph::reconstructPath(vector<int>& vec)
{
   reconstructPathRecursive(vec, goal); 
}


void Graph::printPath()
{
    std::map<int, int>::iterator it = cameFrom.begin();


    for(it; it != cameFrom.end(); ++it)
    {
        cout << it->first <<  "  "<< it->second << endl;
    }    
}


void Graph::setPreviousPath(int node)
{
    cameFrom[node] = current;
}

void Graph::setFScore(int node, int score)
{
    std::map<int, int >::iterator it = fScore.find(node);
    it->second = calculateFScore(score, node);
//cout << it->first <<  "Fscore " << it->second << endl;
}

//return lowest fscore in open list
int Graph::getLowestFScoreNode()
{
    int node = open[0];
    int score;
    score = INT_MAX;

    for(int i = 0; i < open.size(); ++i)
    { 
        std::map<int, int>::iterator it = fScore.find(open[i]);
        if(it->second < score && isNeighbor(it->first)  == true )
        {
            node = it->first;
            score = it->second;
        }
    }
    return node;
}

int Graph::calculateFScore(int gScore, int neighbor)
{

    return gScore + getHeuristic(neighbor);
}

void Graph::setGScore(int node, int score)
{
    std::map<int, int >::iterator it = gScore.find(node);
    it->second = score;
}

void Graph::printGraph()
{
    std::map<int, vector<int> >::iterator it = myGraph.begin();

//    vector<int> vec = it->second;
    for(it; it != myGraph.end(); ++it)
    {
    std::vector<int> vec = it->second;
   // std::vector<vec> iterator it2 = it->second.begin();
//cout << "node " << it->first << "  " ; 
        for(int i = 0; i < vec.size(); ++i)
        {
// cout << "{" << vec[i];
        }
// cout <<"} " <<  endl; 
    }

}

//init graph for face.png, 4x4 matrix
void Graph::initGraph()
{
    start= 0;
    current = 0;
    goal = 15;
    graphSize = 16; //4x4 matrix

    //init matrix for row col data
    //coords for square 0
    vector<int> v;
    v.push_back(0);//row
    v.push_back(0);//col
    matrix.insert(std::pair<int, vector<int> >(0, v) );

    //coords for square 1
    vector<int> v1;
    v1.push_back(0);
    v1.push_back(1);
    matrix.insert(std::pair<int, vector<int> >(1, v1) );

    //coords for square 2
    vector<int> v2;
    v2.push_back(0);
    v2.push_back(2);
    matrix.insert(std::pair<int, vector<int> >(2, v2) );

    //square 3
    vector<int> v3;
    v3.push_back(0);
    v3.push_back(3);
    matrix.insert(std::pair<int, vector<int> > (3, v3) );

    //square 4
    vector<int> v4;
    v4.push_back(1);
    v4.push_back(0);
    matrix.insert(std::pair<int, vector<int> >(4, v4) );

    //square 5
    vector<int> v5;
    v5.push_back(1);
    v5.push_back(1);
    matrix.insert(std::pair<int, vector<int> >(5, v5) );

    //square6 
    vector<int> v6;
    v6.push_back(1);
    v6.push_back(2);
    matrix.insert(std::pair<int, vector<int> >(6, v6) );

    //square7
    vector<int> v7;
    v7.push_back(1);
    v7.push_back(3);
    matrix.insert(std::pair<int, vector<int> >(7, v7) );

    //square8
    vector<int> v8;
    v8.push_back(2);
    v8.push_back(0);
    matrix.insert(std::pair<int, vector<int> > (8, v8) );

    //square 9
    vector<int> v9;
    v9.push_back(2);
    v9.push_back(1);
    matrix.insert(std::pair<int, vector<int> > (9, v9) );

    //square 10;
    vector<int> v10;
    v10.push_back(2);
    v10.push_back(2);
    matrix.insert(std::pair<int, vector<int> >(10, v10) );

    //square 11
    vector<int> v11;
    v11.push_back(2);
    v11.push_back(3);
    matrix.insert(std::pair<int, vector<int> >(11, v11) );

    //square 12
    vector<int> v12;
    v12.push_back(3);
    v12.push_back(0);
    matrix.insert(std::pair<int, vector<int> >(12, v12) );

    //square 13
    vector<int> v13;
    v13.push_back(3);
    v13.push_back(1);
    matrix.insert(std::pair<int, vector<int> >(13, v13) );

    //square 14
    vector<int> v14;
    v14.push_back(3);
    v14.push_back(2);
    matrix.insert(std::pair<int, vector<int> >(14, v14) );

    //square 15
    vector<int> v15;
    v15.push_back(3);
    v15.push_back(3);
    matrix.insert(std::pair<int, vector<int> >(15, v15) );
    std::map<int, vector<int> >::iterator iter = myGraph.begin();

    //INIT myGraph
    //neighbors for node 0
    vector<int> vec;
    vec.push_back(1);
    myGraph.insert(std::pair<int, vector<int> >(0, vec) ); 

    //neighbors for node1 
    vector<int> vec1;
    vec1.push_back(0);
    vec1.push_back(5);
    myGraph.insert(std::pair<int, vector<int> >(1, vec1 ) ); 


    //neighbors for node2 
    vector<int> vec2;
    vec2.push_back(3);
    vec2.push_back(6);
    myGraph.insert(std::pair<int, vector<int> >(2, vec2 ) ); 

    //neighbors for node3 
    vector<int> vec3;
    vec3.push_back(2);
    myGraph.insert(std::pair<int, vector<int> >(3, vec3 ) ); 

    //neighbors for node4 
    vector<int> vec4;
    vec4.push_back(5);
    myGraph.insert(std::pair<int, vector<int> >(4, vec4 ) ); 

    //neighbors for node5 
    vector<int> vec5;
    vec5.push_back(9);
    vec5.push_back(1);
    vec5.push_back(4);
    myGraph.insert(std::pair<int, vector<int> >(5, vec5 ) ); 


    //neighbors for node6 
    vector<int> vec6;
    vec6.push_back(2);
    vec6.push_back(7);
    myGraph.insert(std::pair<int, vector<int> >(6, vec6 ) ); 

    //neighbors for node7 
    vector<int> vec7;
    vec7.push_back(6);
    vec7.push_back(11);
    myGraph.insert(std::pair<int, vector<int> >(7, vec7 ) ); 

    //neighbors for node8 
    vector<int> vec8;
    vec8.push_back(9);
    vec8.push_back(12);
    myGraph.insert(std::pair<int, vector<int> >(8, vec8 ) ); 

    //neighbors for node9 
    vector<int> vec9;
    vec9.push_back(5);
    vec9.push_back(10);
    vec9.push_back(8);
    vec9.push_back(13);
    myGraph.insert(std::pair<int, vector<int> >(9, vec9 ) ); 


    //neighbors for node10 
    vector<int> vec10;
    vec10.push_back(9);
    vec10.push_back(11);
    myGraph.insert(std::pair<int, vector<int> >(10, vec10 ) ); 

    //neighbors for node11 
    vector<int> vec11;
    vec11.push_back(7);
    vec11.push_back(10);
    vec11.push_back(15);
    myGraph.insert(std::pair<int, vector<int> >(11, vec11 ) ); 

    //neighbors for node12 
    vector<int> vec12;
    vec12.push_back(8);
    vec12.push_back(13);
    myGraph.insert(std::pair<int, vector<int> >(12, vec12 ) ); 

    //neighbors for node13 
    vector<int> vec13;
    vec13.push_back(9);
    vec13.push_back(14);
    myGraph.insert(std::pair<int, vector<int> >(13, vec13 ) ); 

    //neighbors for node14 
    vector<int> vec14;
    vec14.push_back(13);
    myGraph.insert(std::pair<int, vector<int> >(14, vec14 ) ); 

    //neighbors for node15 
    vector<int> vec15;
    vec15.push_back(11);
    myGraph.insert(std::pair<int, vector<int> >(15, vec15 ) ); 
}

