#include <map>
#include<queue>
#include <vector>
#ifndef GRAPH_H

using namespace std;

class Graph
{
  public:
    Graph();
    ~Graph(){};
    int getNextMove();
    vector<int> open;//neighbor nodes to visit, init to 0 val for each node, if 1 then in set
    vector<int> closed;//nodes that have been visited, init to 0 for each node, if 1 in set
    bool aStar();//a* pathfinding algorithm
    int getGoal();//return goal node
    bool isGoalNode();//return true if goal hit    
    map<int, int> fScore;//for each node, the total cost of getting from start to goal nodes
    map<int, int> gScore;//cost of getting from start node to current node
    map<int, int> cameFrom;//for each node, contains previous node(came from)  
                                 //ex: key= to, val = from so, first entry = key:nextNode, val:start 
    map<int, vector<int> > matrix;//contains row,col for each square(ex: 0= row 0, col 0)
    map<int, vector<int> > myGraph;//contains graph data 
    void setFScore(int, int);
    int getLowestFScoreNode();
    void setGScore(int, int);
    int getHeuristic(int);
    int getGScore(int );
    void initGScore();
    void initFScore();
    void initGraph();
    void initGameState();
    int calculateFScore(int, int);
    void setPreviousPath(int);
    void printPath();
    void reconstructPath(vector<int>&);
    bool isOpen(int);
    bool isClosed(int);
    void removeOpenNode(int);
    void printGraph();
    bool isNeighbor(int);
    vector<int> gameEngine();
    int getParent(int);
    void reconstructPathRecursive(vector<int>&, int);
  private:
    int start;
    int goal;
    int current;
    int graphSize; 
};
#endif
