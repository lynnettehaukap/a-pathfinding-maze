//To compile: g++ -o myProg main.cpp Graph.cpp `sdl2-config --cflags --libs`

#include <iostream>
#include <istream>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include "Graph.h"
#include <map>
#include <vector>
#include <list>
#include <iomanip>
#include <fstream>
#include <assert.h>
#include <string.h>
#include <cstdlib>
#include <string>

void setRectLocation( SDL_Rect&, vector<int>&, int&);
void setRect(int, int&, int&, int&, int&);

using namespace std;
int main(int argc, char* argv[])
{
    char file[] = "face.bmp"; 
    char* fileName = file;
    char file2[] = "start.bmp"; 
    char* fileName2 = file2;
    char file3[] = "goal.bmp"; 
    char* fileName3 = file3;
    Graph graph;
    int index = 0;
   
    vector<int> vec = graph.gameEngine();
    for(int i = vec.size() -1; i >= 0; --i)
    {
        cout << vec[i] << endl;
    }
    //set index to last element of a* path vector(aka start node)
    index = vec.size()-1;

    SDL_Window* window;
    SDL_Init(SDL_INIT_VIDEO);

    //IMG_Init(IMG_INIT_PNG);

  // create window 
    window = SDL_CreateWindow("", 
                            0, 
                            0,
                            600, 600, 

                            SDL_WINDOW_INPUT_FOCUS);


    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
    //declare image
    SDL_Surface* image = NULL;
    //declare textures
    SDL_Texture *texture = NULL;
    SDL_Texture *start = NULL;
    SDL_Texture *goal = NULL;
    
    //load graph image
    image = SDL_LoadBMP(fileName);
    //handle image load errors
    if (image == NULL)
    {
        printf("Unable to load bitmap: %s\n", SDL_GetError());
        return 2;
    }

    //create texture and free surface
    texture = SDL_CreateTextureFromSurface( renderer, image);
    SDL_FreeSurface(image);

    //load start image
    image = SDL_LoadBMP(fileName2);
    //handle image load errors
    if (image == NULL)
    {
        printf("Unable to load bitmap: %s\n", SDL_GetError());
        return 2;
    }

    //create textures and free surfaces
    start = SDL_CreateTextureFromSurface( renderer, image);
    SDL_FreeSurface(image);

    //load goal image
    image = SDL_LoadBMP(fileName3);
    //handle image load error
    if (image == NULL)
    {
        printf("Unable to load bitmap: %s\n", SDL_GetError());
        return 2;
    }

    //create textures and free surfaces
    goal = SDL_CreateTextureFromSurface( renderer, image);
    SDL_FreeSurface(image);
    
    SDL_RenderClear(renderer);

    SDL_Event event;

    int gameover = 0;

  
    while (!gameover)
    {
    
        if (SDL_PollEvent(&event))
        {
       
            switch (event.type) 
            {
        
                case SDL_QUIT:
                gameover = 1;
                break;

                case SDL_KEYDOWN:
                switch (event.key.keysym.sym) 
                {
                    case SDLK_ESCAPE:
                    case SDLK_q:
                    gameover = 1;
                    break;
                }
                break;
            }
        }
        SDL_RenderCopy(renderer, texture, NULL, NULL);
        SDL_RenderPresent(renderer);

        SDL_Rect destRect;
        {
            if(index > -1)
            {
cerr << "index " <<index << endl;
//                if(index == vec.size() -1)
  //                  SDL_Delay(2000);   
                    

                setRectLocation(destRect, vec, index);
            }
            if(index == -1)
            {
                SDL_Delay(200);   
                //reset index to retrace path
                index = vec.size()-1;
            }
        }
    //    SDL_Rect destRect = {x, y, w, h};
        //SDL_Rect destRect = {480, 270, 960, 540};
        //SDL_Rect destRect1 = {0, 0, 480, 270};
        SDL_RenderCopy(renderer, start, NULL, &destRect);
        

        SDL_Rect destRect2 = {450, 450, 150,150};
        SDL_RenderCopy(renderer, goal, NULL, &destRect2);
        SDL_RenderPresent(renderer);
        SDL_Delay(200);   
  }
    //end while loop
    SDL_DestroyTexture(texture);
    SDL_DestroyTexture(start);
    SDL_DestroyTexture(goal);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

void setRect(int n, int& x, int& y, int& w, int& h)
{
    switch(n){
    case(0) :
    x = 0;
    y = 0;
    w = 480;
    h = 270;
    break;
    default: cout << "n" << n << endl;
    }
}

void setRectLocation(SDL_Rect& r, vector<int>& vec,  int& index)
{
    int path = vec[index]; 
    switch(path) {
        case(0) :
                  r.x = 0;
                  r.y = 0;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(1) : r.x = 150;
                  r.y = 0;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(2) : r.x = 300;
                  r.y = 0;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(3) : r.x = 450;
                  r.y = 0;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(4) : r.x = 0;
                  r.y = 150;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(5) : r.x = 150;
                  r.y = 150;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(6) : r.x = 300;
                  r.y = 150;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(7) : r.x = 450;
                  r.y = 150;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(8) : r.x = 0;
                  r.y = 300;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(9) : r.x = 150;
                  r.y = 300;
                  r.w = 125;
                  r.h = 125;
                  --index;
                  break;
        case(10) : r.x = 300;
                   r.y = 300;
                   r.w = 125;
                   r.h = 125;
                   --index;
                  break;
        case(11) : r.x = 450;
                   r.y = 300;
                   r.w = 125;
                   r.h = 125;
                   --index;
                  break;
        case(12) : r.x = 0;
                   r.y = 450;
                   r.w = 125;
                   r.h = 125;
                   --index;
                  break;
        case(13) : r.x = 150;
                   r.y = 450;
                   r.w = 125;
                   r.h = 125;
                   --index;
                  break;
        case(14) : r.x = 300;
                   r.y = 450;
                   r.w = 125;
                   r.h = 125;
                   --index;
                  break;
        case(15) : r.x = 450;
                   r.y = 450;
                   r.w = 125;
                   r.h = 125;
                   --index;
                  break;
        default : --index;; 
                  break;
    }
}
