# README #

This project is an A* pathfinding algorithm and executes using hardcoded mazes.

Runs on Ubuntu 16.04 and compiles using g++
SDL2 must be installed.
To compile: g++ main.cpp Graph.cpp `sdl2-config --cflags --libs`
To run: ./a.out

Future works: modify to take an maze files as parameters